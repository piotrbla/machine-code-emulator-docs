# Calculate first n-th (n > 0) fibonacci's numbers
out "Enter amount of numbers to calculate", term
in r0, term

# Set first fibonacci's number
mov r1, 0

# Set second fibonacci's number
mov r2, 1

# Set first fibonacci's number in memory
mov $0, r1

# Set cursor in memory
mov r3, 1

mov %r3, r2
add %r3, r1
mov r1, r2
mov r2, %r3
add r3, 1

# Decrement iterator
sub r0, 1
jnz 16
